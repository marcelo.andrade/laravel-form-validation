<?php
const MARITAL_STATUS = [
    1 => 'Solteiro',
    2 => 'Casado',
    3 => 'Divorciado',
];