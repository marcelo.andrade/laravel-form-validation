<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function (){

/** +--------+-----------+-----------------------------+-----------------+------------------------------------------------------+--------------+
| Domain | Method    | URI                         | Name            | Action                                               | Middleware   |
    +--------+-----------+-----------------------------+-----------------+------------------------------------------------------+--------------+
|        | GET|HEAD  | /                           |                 | Closure                                              | web          |
|        | GET|HEAD  | admin/clients               | clients.index   | App\Http\Controllers\Admin\ClientsController@index   | web          |
|        | POST      | admin/clients               | clients.store   | App\Http\Controllers\Admin\ClientsController@store   | web          |
|        | GET|HEAD  | admin/clients/create        | clients.create  | App\Http\Controllers\Admin\ClientsController@create  | web          |
|        | GET|HEAD  | admin/clients/{client}      | clients.show
    *    | App\Http\Controllers\Admin\ClientsController@show    | web          |
|        | PUT|PATCH | admin/clients/{client}      | clients.update  | App\Http\Controllers\Admin\ClientsController@update  | web          |
|        | DELETE    | admin/clients/{client}      | clients.destroy | App\Http\Controllers\Admin\ClientsController@destroy | web          |
|        | GET|HEAD  | admin/clients/{client}/edit | clients.edit    | App\Http\Controllers\Admin\ClientsController@edit    | web          |
|        | GET|HEAD  | api/user                    |                 | Closure                                              | api,auth:api |
    +--------+-----------+-----------------------------+-----------------+------------------------------------------------------+--------------+ **/
    Route::resource('clients', 'ClientsController');

    /** continuar estudos sobre o DataTable
    Route::get('datatable/getdata', 'DataTableController@getPosts')->name('datatable/getdata');
     * **/
});
/** Costrucao de uma rota nomeada*/
Route::get('/qualquer-coisa', function (){
    echo 'Hello World!';
})->name('nomeada');
