@extends('layouts.layout')

@section('content')
    <div class="panel panel-default panel-primary">
        <div class="panel-heading">
            <h3>Novo Cliente</h3>
        </div>
        @include('layouts._errors')
        <div class="panel-body">
            <form action="{{URL::to('admin/clients')}}" method="post">{{csrf_field()}}
                @include('layouts._form-client')
                <div class="buttons pull pull-right">
                    <a href="{{route('clients.index')}}" class="btn btn-default">Cancelar</a>
                    <input type="reset" name="limpar" id="limpar" class="btn btn-primary" value="Limpar"/>
                    <input type="submit" name="salvar_cliente" id="salvar_cliente" class="btn btn-success" value="Salvar"/>
                </div>
            </form>
        </div>
        <div class="panel-footer text-center">
            <p>&circledR;Direitos reservados</p>
        </div>
    </div>
@stop