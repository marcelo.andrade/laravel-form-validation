@extends('layouts.layout')

@section('content')
    <div class="panel panel-default panel-primary">
        <div class="panel-heading">
            <h3>Alterar Cliente / <span class="small" style="color: white">{{$client->name}}</span></h3>
        </div>
        @include('layouts._errors')
        <div class="panel-body">
            <form action="{{route('clients.update', $client->id)}}" method="post">
                {{csrf_field()}}
                {{--funcao que define qual tipo de método de envio será assumido após o post, nesse caso o método é PUT--}}
                {{method_field('PUT')}}
                @include('layouts._form-client')
                <div class="buttons pull pull-right">
                    <a href="{{route('clients.index')}}" class="btn btn-default">Cancelar</a>
                    <input type="submit" name="salvar_cliente" id="salvar_cliente" class="btn btn-success" value="Salvar"/>
                </div>
            </form>
        </div>
        <div class="panel-footer text-center">
            <p>&circledR;Direitos reservados</p>
        </div>
    </div>
@stop