@extends('layouts.layout')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3>Lista de Clientes</h3>
            <div class="text-right">
                {{--referencia utilizadno rota padrao--}}
                {{--<a href="/admin/clients/create" class="btn btn-primary">Novo cliente</a>--}}
                {{--refenrencia utilizando rota nomeada--}}
                <a href="{{route('clients.create')}}" class="btn btn-default">Novo cliente</a>
            </div>
        </div>
        <div class="panel-body">
            <table id="example" class="table table-responsive table-bordered" style="width:100%">
                <thead>
                <tr class="text-right">
                    <th width="2%">Id</th>
                    <th width="18%">Nome</th>
                    <th width="15%">E-mail</th>
                    <th width="15%">Telefone</th>
                    <th width="5%">Inad.</th>
                    <th width="10%">Data de nasc.</th>
                    <th width="5%">Sexo</th>
                    <th width="10%">Estado Civil</th>
                    <th width="5%">Defic. Física</th>
                    <th width="15%">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr style="text-align: left">
                        <td>{{$client->id}}</td>
                        <td>{{$client->name}}</td>
                        <td>{{$client->email}}</td>
                        <td>{{$client->phone}}</td>
                        <td>{{$client->defaulter}}</td>
                        <td>{{Carbon\Carbon::parse($client->date_birth)->format('d-m-Y')}}</td>
                        <td>{{$client->sex}}</td>
                        <td>{{$client->marital_status}}</td>
                        <td>{{$client->physical_disability}}</td>
                        {{--<td><a href="{{route('clients.edit', ['client' => $client->id])}}">Editar</a></td>--}}
                        {{--<td><a href="{{URL::to('/admin/clients/' . $client->id  . '/edit')}}">Editar</a></td>--}}
                        <td>
                            {{--<a href="{{route('clients.edit', $client->id)}}">Editar</a>--}}
                            {{--<a href="{{route('clients.show', $client->id)}}" class="btn btn-primary">Detalhes</a>--}}
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <a href="{{route('clients.show', $client->id)}}" class="btn btn-primary">Detalhes</a>
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        &blacktriangledown;
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="{{route('clients.edit', $client->id)}}">Editar</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="text-center">
        {{$clients->fragment('clientes')->links()}}
    </div>
    {{--construção das modals--}}
    <div id="cliente-detalhes" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalhes do cliente / {{ $client->name }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>chamar conteudo aqui</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@stop