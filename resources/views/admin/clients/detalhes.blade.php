@extends('layouts.layout')

@section('content')
    <form id="form-delete" method="post" action="{{route('clients.destroy', $client->id)}}">
        {{csrf_field()}}{{method_field('DELETE')}}
    </form>
    <div class="panel panel-default panel-primary">
        <div class="panel-heading">
            <h3>Detalhes / <span class="small" style="color: white">{{$client->name}}</span></h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped table-responsive ">
                <tbody>
                <tr>
                    <th scope="row">Nome</th>
                    <td>{{$client->name}}</td>
                </tr>
                <tr>
                    <th scope="row">E-mail</th>
                    <td>{{$client->email}}</td>
                </tr>
                <tr>
                    <th scope="row">Telefone</th>
                    <td>{{$client->phone}}</td>
                </tr>
                <tr>
                    <th scope="row">Inadimplente</th>
                    <td>{{$client->defaulter > 0 ? 'Sim' : 'Não'}}</td>
                </tr>
                <tr>
                    <th scope="row">Data de nascimento</th>
                    <td>{{$client->date_birth}}</td>
                </tr>
                <tr>
                    <th scope="row">Sexo</th>
                    <td>{{$client->sex}}</td>
                </tr>
                <tr>
                    <th scope="row">Estado Cível</th>
                    <td>
                        <?php switch ($client->marital_status) {
                            case 1:
                                echo 'Solteiro (a)';
                                break;
                            case 2:
                                echo 'Casado (a)';
                                break;
                            case 3:
                                echo 'Divorciado (a)';
                                break;
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Deficiência Física</th>
                    <td>{{ $client->physical_disability }}</td>
                </tr>
                </tbody>
            </table>
            <div class="buttons pull pull-right">
                <a href="{{route('clients.index')}}" class="btn btn-default">Voltar</a>
                <a href="{{route('clients.edit', $client->id)}}" class="btn btn-primary">Alterar</a>
                <a href="{{route('clients.destroy', $client->id)}}"
                   class="btn btn-danger"
                   onclick="event.preventDefault(); document.getElementById('form-delete').submit();">
                   Excluir
                </a>
            </div>
        </div>
        <div class="panel-footer text-center">
            <p>&circledR;Direitos reservados</p>
        </div>
    </div>
@stop