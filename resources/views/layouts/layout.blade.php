<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">--}}
    <title>Laravel Form Validation</title>
</head>
<body>
<div class="jumbotron text-center">
    <h2>Laravel Form Validation</h2>
    <p>Resize this responsive page to see the effect!</p>
</div>
<div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12">
        @yield('content')
    </div>
</div>
<div class="footer jumbotron text-center">
    &copy; Laravel Form Valitation
</div>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jQuery.js')}}"></script>
{{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>--}}
</body>
</html>