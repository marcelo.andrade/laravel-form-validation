<div class="row">
    <div class="form-group col-md-6">
        <label for="name">Nome</label>
        <input type="text" class="form-control" id="name" name="name" value="{{old('name', $client->name)}}">
    </div>
    <div class="form-group col-md-6">
        <label for="email">E-mail</label>
        <input type="email" class="form-control" id="email" name="email" value="{{old('email', $client->email)}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-2">
        <label for="phone">Telefone</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{$client->phone}}">
    </div>
    <div class="form-group col-md-3">
        <label for="date_birth">Data de nascimento</label>
        <input type="date" class="form-control" id="date_birth" name="date_birth" value="{{$client->date_birth}}">
    </div>
    <div class="form-group col-md-3">
        <label for="marital_status">Estado Civil:</label>
        <select name="marital_status" id="marital_status" class="form-control">
            <option value="0">Selecione o estado civil</option>
            <option value="1" {{$client->marital_status == 1 ? 'selected' : ''}}>Solteiro</option>
            <option value="2" {{$client->marital_status == 2 ? 'selected' : ''}}>Casado</option>
            <option value="3" {{$client->marital_status == 3 ? 'selected' : ''}}>Divorciado</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="radio">
            <b>Sexo:</b> &emsp;
            <label>
                <input type="radio" name="sex" value="f" {{$client->sex == 'f' ? 'checked' : ''}}>Feminino
            </label>&emsp;
            <label>
                <input type="radio" name="sex" value="m" {{$client->sex == 'm' ? 'checked' : ''}}>Masculino
            </label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="checkbox checkbox-primary">
            <b>Situação financeira:</b>
            <label>
                <input type="checkbox" name="defaulter" value="1" {{$client->defaulter ? 'checked' : ''}}>Inadimplente
            </label>
        </div>
    </div>
    <div class="form-group col-md-6">
        <label for="physical_disability">Deficiencia física? Qual?</label>
        <input type="text" name="physical_disability" id="physical_disability" class="form-control">
    </div>
</div>

