@if($errors->any())
    <div class="alert">
        <ul class="alert alert-danger">
            @foreach($errors->all() as $erro)
                <li>
                    <b>{{$erro}}</b>
                </li>
            @endforeach
        </ul>
    </div>
@endif
