<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client)
    {
    /** selecionando nome onde registro começa com Granet e selecionando
        $clients = $client->where('name','like', 'Garnet%')->get();
    **/
        $clients = $client->orderBy('id', 'desc')->paginate(10);
        return view('admin.clients.clients', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /** criacao de cliente vazio para atender a visualizacao que instancia da model Client*/
        $client = new Client();
        return view('admin.clients.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** validação dos campos */
        $this->_validate($request);
        $data = $request->all();
        $data['physical_disability'] = $request->physical_disability == null ? 'N' : $request->physical_disability ;
        $data['defaulter'] = $request->has('defaulter');
        /**
        $data['physical_disability'] = $request->has('physical_disability');
        $data['defaulter'] = $request->has('defaulter');
        **/
        Client::create($data);
        return redirect('admin/clients');
    }

    /**
     * Display the specified resource.
     *
     * param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('admin.clients.detalhes', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * param  int  $client
     * return \Illuminate\Http\Response
     */
    public function edit(Client $client) /** utilizando a consulta diretamente com Route Model Binding*/
    {
        /**Com Route Model Binding nao se utiliza findOrFail($id), ou seja, basta deletar a linha abaixo:
        $client = Client::findOrFail($client);
         */
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $this->_validate($request);
        $data = $request->all();
        $data['physical_disability'] = $request->physical_disability == null ? 'N' : $request->physical_disability ;
        $data['defaulter'] = $request->has('defaulter');
        /** Preenchimento dos campos pertinentes definidos nos fillables */
        $client->fill($data);
        $client->save();

        return redirect()->route('clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return redirect()->route('clients.index', compact('client'));
    }

    public function getPosts()
    {
        return \DataTables::of(Client::query())->make(true);
    }

    /** Valida os registros enviados do formulário*/
    protected function _validate(Request $request){
        $marital_status = implode(',', array_keys(Client::MARITAL_STATUS));
        $this->validate($request, [
            'name' => 'required|max: 191',
            'email' => 'required|email',
            'phone' => 'required',
            'date_birth' => 'required|date',
            'sex' => 'required|in:m,f',
            'marital_status' => "required|in:$marital_status",
            'physical_disability' => 'max:191',
        ]);

    }
}
